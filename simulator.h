#ifndef _SIMULATOR_H_
#define _SIMULATOR_H_

#include <vector>
#include "common.h"

const float WORLD_HEIGHT = 18.0;
const float WORLD_WIDTH = 24.0;
const float CAR_LENGTH = 3.0;
const float CAR_WIDTH = 1.5;
const float CAR_TURNING_RADIUS = 5.0;

class Car: public Point3D {
public:
    Car() {}
    Car(float _x, float _y, float _heading): Point3D(_x, _y, _heading) {}

    void getCorners(float* corners_x, float* corners_y) const {
        float half_length_x = 0.5 * CAR_LENGTH * cos(heading);
        float half_length_y = 0.5 * CAR_LENGTH * -sin(heading);
        float half_width_x = 0.5 * CAR_WIDTH * cos(heading + PI/2);
        float half_width_y = 0.5 * CAR_WIDTH * -sin(heading + PI/2);
        corners_x[0] = x + half_length_x + half_width_x;
        corners_x[1] = x + half_length_x - half_width_x;
        corners_x[2] = x - half_length_x - half_width_x;
        corners_x[3] = x - half_length_x + half_width_x;
        corners_y[0] = y + half_length_y + half_width_y;
        corners_y[1] = y + half_length_y - half_width_y;
        corners_y[2] = y - half_length_y - half_width_y;
        corners_y[3] = y - half_length_y + half_width_y;
    }
    void getCorners(Point2D* corners) const {
        float half_length_x = 0.5 * CAR_LENGTH * cos(heading);
        float half_length_y = 0.5 * CAR_LENGTH * -sin(heading);
        float half_width_x = 0.5 * CAR_WIDTH * cos(heading + PI/2);
        float half_width_y = 0.5 * CAR_WIDTH * -sin(heading + PI/2);
        corners[0] = Point2D(x + half_length_x + half_width_x, y + half_length_y + half_width_y);
        corners[1] = Point2D(x + half_length_x - half_width_x, y + half_length_y - half_width_y);
        corners[2] = Point2D(x - half_length_x - half_width_x, y - half_length_y - half_width_y);
        corners[3] = Point2D(x - half_length_x + half_width_x, y - half_length_y + half_width_y);
    }
};

class Simulator {
public:
    Car car;
    Point3D target;
    std::vector<float> obstacles_x;
    std::vector<float> obstacles_y;

    Simulator();
    ~Simulator();

    void loadMap(const char* filename);
    bool obsFree(const Point3D& point);
    bool obsFree(const Car& projected_car);
private:
};

#endif
