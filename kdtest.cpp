#include <vector>
#include <iostream>
#include <random>
#include <algorithm>
#include "common.h"
#include "kdtree.h"

struct LPoint {
    Point2D point;
    float dist;

    bool operator<(const LPoint& other) const {
        return dist < other.dist;
    }
};

const int NUM_POINTS = 300000;
const int NUM_TESTS = 30;
const float EPSILON = 0.001;

int main() {
    std::mt19937 gen;
    std::uniform_real_distribution<float> uniform(-100.0, 100.0);

    // Generate some points
    std::vector<Point2D> points;
    for(int i=0; i<NUM_POINTS; i++) {
        points.push_back(Point2D(uniform(gen), uniform(gen)));
    }

    // Build kd-tree
    KDNode kdtree(Point2D(uniform(gen), uniform(gen)), false);

    bool insertion_failed = false;
    #pragma omp parallel for num_threads(32)
    for(int i=0; i<NUM_POINTS; i++) {
        // Actual points
        if ((kdtree.insert(points[i]))->dist(points[i]) > EPSILON) {
            insertion_failed = true;
        }
        kdtree.insert(Point2D(uniform(gen), uniform(gen)), false);
    }
    if (insertion_failed) {
        std::cout << "Insertion return failure!" << std::endl;
        return 0;
    }

    // Copy of points
    std::vector<LPoint> lpoints;
    for(int i=0; i<NUM_POINTS; i++) {
        LPoint lpoint;
        lpoint.point = points[i];
        lpoints.push_back(lpoint);
    }

    // Run the tests
    for(int i=1; i<=NUM_TESTS; i++) {
        Point2D target(uniform(gen), uniform(gen));

        // NN provided by kd-tree
        std::vector<Point2D*> kd_nearest = kdtree.nearest_neighbours(target, i*10);

        // Range provided by kd-tree
        const float test_range = static_cast<float>(i)*3.0;
        std::vector<Point2D*> kd_in_range = kdtree.neighbours_within_range(target, test_range);
        std::vector<LPoint> kd_range_lpoints;
        for(int j=0; j<kd_in_range.size(); j++) {
            LPoint lpoint;
            lpoint.point = *(kd_in_range[j]);
            lpoint.dist = lpoint.point.dist(target);
            kd_range_lpoints.push_back(lpoint);
        }
        std::sort(kd_range_lpoints.begin(), kd_range_lpoints.end());

        // All points, sorted by distance to target
        for(int j=0; j<NUM_POINTS; j++) {
            lpoints[j].dist = lpoints[j].point.dist(target);
        }
        std::sort(lpoints.begin(), lpoints.end());

        // Make sure they're all correct
        bool failed = false;
        // Ensure nearest neighbours are indeed the nearest
        for(int j=0; j<kd_nearest.size(); j++) {
            float error = kd_nearest[j]->dist(lpoints[j].point);
            if (error > EPSILON) {
                failed = true;
                break;
            }
        }
        // Make sure everything returned by range is indeed nearest
        for(int j=0; j<kd_range_lpoints.size(); j++) {
            float error = kd_range_lpoints[j].point.dist(lpoints[j].point);
            if (error > EPSILON) {
                failed = true;
                break;
            }
        }
        // Make sure range didn't miss out anything
        if (lpoints.size() > kd_range_lpoints.size()) {
            if (lpoints[kd_range_lpoints.size()].point.dist(target) < test_range) {
                failed = true;
            }
        }
        std::cout << "Test " << i << " " << (failed ? "FAILED" : "passed") << "\n";
    }

    return 0;
}