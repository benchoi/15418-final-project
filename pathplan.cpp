#include <cstdlib>
#include <cassert>
#include <float.h>
#include <iostream>
#include <omp.h>
#include "pathplan.h"
#include "CycleTimer.h"

const int RRT_ITERATIONS = 200000;
const int NUM_THREADS = 2;
const int NUM_NEAREST = 10;

const float DT = 0.5;
const float MAX_STEER = PI/4;

double avg(double* timings) {
    double total = 0.0;
    for(int i=0; i<NUM_THREADS; i++) {
        total += timings[i];
    }
    return total / static_cast<double>(NUM_THREADS);
}

// Trying to port over function from
// http://msl.cs.uiuc.edu/~lavalle/cs576_1999/projects/junqu/
// vn is equivalent to cur_nearest, q is rand_point
NodeInfo optimum_control(NodeInfo vn, NodeInfo q) {
    float vn_x = vn.x;
    float vn_y = vn.y;
    float vn_h = -vn.heading;
    float q_x = q.x;
    float q_y = q.y;
    float q_h = -q.heading;
    
    float dr, d=FLT_MAX;
    NodeInfo new_point;
    NodeInfo best_point;

    float steer[3];
    int v[2]={1, -1};
    
    steer[0]=atan(CAR_LENGTH*(vn_h-q_h)/ ((vn_x-q_x)*cos(vn_h)+(vn_y-q_y)*sin(vn_h)));
    steer[1]=atan(CAR_LENGTH*(vn_h-q_h+2*PI)/ ((vn_x-q_x)*cos(vn_h)+(vn_y-q_y)*sin(vn_h)));
    steer[2]=atan(CAR_LENGTH*(vn_h-q_h-2*PI)/ ((vn_x-q_x)*cos(vn_h)+(vn_y-q_y)*sin(vn_h)));
    
    for (int i=0;i<2;i++){
        for (int j=0;j<3;j++){
            if (steer[j] < -MAX_STEER)
                steer[j] = -MAX_STEER;
            else if (steer[j] > MAX_STEER)
                steer[j] = MAX_STEER;
            
            float x = vn_x+v[i]*DT*cos(vn_h);//*cos(steer[j]);
            float y = vn_y+v[i]*DT*sin(vn_h);//*cos(steer[j]);
            float heading = vn_h+(v[i]*DT/CAR_LENGTH)*sin(steer[j]);
            new_point = NodeInfo(x, y, -heading, 0.0, NULL);
            
            dr=dist(new_point, q);
            if(dr<d){
                d=dr;
                best_point = new_point;
            }       
        }
    }
    return best_point;
}

inline float dist(NodeInfo p, NodeInfo q) {
    float d;
    float dtheta;
    
    dtheta=p.heading-q.heading;
    if (dtheta>PI)
        dtheta-=2*PI;
    else if (dtheta<-PI)
        dtheta+=2*PI;
    
    d = (p.x-q.x)*(p.x-q.x)+ (p.y-q.y)*(p.y-q.y)+ CAR_LENGTH*CAR_LENGTH*dtheta*dtheta;
    d = pow(d,0.5);
    
    return(d);
}

void PathPlanner::plan() {
    // Clear previous tree, if any
    if (tree != NULL) {
        delete tree;
    }

    double overall_start_time = CycleTimer::currentSeconds();
    double time_kdtree[NUM_THREADS] = {};
    double time_find_best[NUM_THREADS] = {};
    double time_obsfree[NUM_THREADS] = {};
    double time_kdtree_insert[NUM_THREADS] = {};

    // Build kdtree and insert some structural points
    tree = new KDNode(NodeInfo(0.5 * WORLD_WIDTH, 0.5 * WORLD_HEIGHT, 0.0), false);
    tree->insert(NodeInfo(0.25 * WORLD_WIDTH, 0.5 * WORLD_HEIGHT, 0.0), false);
    tree->insert(NodeInfo(0.75 * WORLD_WIDTH, 0.5 * WORLD_HEIGHT, 0.0), false);

    // Insert root node of RRT
    tree->insert(NodeInfo(sim->car.x, sim->car.y, sim->car.heading));

    // RRT
    int iteration_counts[NUM_THREADS] = {};

    #pragma omp parallel for num_threads(NUM_THREADS)
    for(int i=0; i<RRT_ITERATIONS; i++) {
        const int num_threads = omp_get_num_threads();
        const int tid = omp_get_thread_num();
        double start_time;

        // Increment iteration count
        iteration_counts[tid]++;
        if (iteration_counts[tid] % 25000 == 0) {
            std::cout << "Thread " << tid << " is at " << iteration_counts[tid] << " iterations" << std::endl;
        }

        // Randomly choose a point
        NodeInfo rand_point;
        const float rand_heading = drand48() * 2 * PI;
        if (num_threads == 1) {
            rand_point = NodeInfo(drand48() * WORLD_WIDTH, drand48() * WORLD_HEIGHT, rand_heading);
        }
        else if (num_threads == 2) {
            float x_offset = (tid % 2 == 0 ? 0.0 : 0.5 * WORLD_WIDTH);
            rand_point = NodeInfo(x_offset + drand48() * 0.5 * WORLD_WIDTH, 
                drand48() * WORLD_HEIGHT, rand_heading);
        }
        else if (num_threads == 4) {
            float x_offset = (tid % 2 == 0 ? 0.0 : 0.5 * WORLD_WIDTH);
            float y_offset = (tid / 2 == 0 ? 0.0 : 0.5 * WORLD_HEIGHT);
            rand_point = NodeInfo(x_offset + drand48() * 0.5 * WORLD_WIDTH, 
                y_offset + drand48() * 0.5 * WORLD_HEIGHT, rand_heading);
        }

        // Find closest node in tree
        start_time = CycleTimer::currentSeconds();
        std::vector<NodeInfo*> nearest = tree->nearest_neighbours(rand_point, NUM_NEAREST);
        time_kdtree[tid] += CycleTimer::currentSeconds() - start_time;

        // Copy out contents of pointers (optimization to avoid false sharing)
        std::vector<NodeInfo> nearest_copy(nearest.size());
        for(size_t j=0; j<nearest.size(); j++) {
            nearest_copy[j] = *(nearest[j]);
        }

        // Find the best new point
        bool found_feasible_node = false;
        float best_dist = FLT_MAX;
        NodeInfo best_point;
        start_time = CycleTimer::currentSeconds();
        for(size_t j=0; j<nearest.size(); j++) {
            const NodeInfo& cur_nearest = nearest_copy[j];

            NodeInfo new_point = optimum_control(cur_nearest, rand_point);
            float new_point_dist = dist(new_point, rand_point);
            if (new_point_dist < best_dist){
                best_dist = new_point_dist;
                best_point = new_point;
                best_point.parent = nearest[j];
                found_feasible_node = true;
            }
        }
        time_find_best[tid] += CycleTimer::currentSeconds() - start_time;
        if (!found_feasible_node) {
            continue;
        }

        // Obstacle check
        start_time = CycleTimer::currentSeconds();
        bool unobstructed = sim->obsFree(Car(best_point.x, best_point.y, best_point.heading));
        time_obsfree[tid] += CycleTimer::currentSeconds() - start_time;
        if (!unobstructed) {
            continue;
        }

        // Insert new point into the RRT
        start_time = CycleTimer::currentSeconds();
        tree->insert(best_point);
        time_kdtree_insert[tid] += CycleTimer::currentSeconds() - start_time;
    }

    std::cout << "Overall time: " << (CycleTimer::currentSeconds() - overall_start_time) << std::endl;
    std::cout << "Avg time spent on kdtree search: " << avg(time_kdtree) << std::endl;
    std::cout << "Avg time spent on finding best new point: " << avg(time_find_best) << std::endl;
    std::cout << "Avg time spent on obstacle checking: " << avg(time_obsfree) << std::endl;
    std::cout << "Avg time spent on kdtree insert: " << avg(time_kdtree_insert) << std::endl;
}
