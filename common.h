#ifndef _COMMON_H_
#define _COMMON_H_

#include <cmath>
#include <cstddef>
#include <limits>
#include <algorithm>

#ifndef FLT_MAX
const float FLT_MAX = std::numeric_limits<float>::max();
#endif
const float PI = 3.1415926535897932384626433832795;

inline int sqr(int x) { return x*x; }
inline float sqr(float x) { return x*x; }

class Point2D {
public:
    float x;
    float y;

    Point2D() {}
    Point2D(float _x, float _y): x(_x), y(_y) {}

    float sq_dist(const Point2D& other) const {
        return sqr(x - other.x) + sqr(y - other.y);
    }

    float dist(const Point2D& other) const {
        return sqrt(sq_dist(other));
    }
};

inline float sq_dist(const Point2D& p0, const Point2D& p1) {
    return p0.sq_dist(p1);
}
inline float dist(const Point2D& p0, const Point2D& p1) {
    return p0.dist(p1);
}

class Point3D: public Point2D {
public:
    float heading;
    
    Point3D() {}
    Point3D(float _x, float _y, float _heading): Point2D(_x, _y), heading(_heading) {}
};

struct NodeInfo: public Point3D {
    float cost;
    NodeInfo* parent;
    
    NodeInfo() {}
    NodeInfo(const Point3D& point): Point3D(point), cost(0.0), parent(NULL) {}
    NodeInfo(float _x, float _y, float _heading, float _cost = 0.0, NodeInfo* _parent = NULL): 
        Point3D(_x, _y, _heading), cost(_cost), parent(_parent) {}
};

class Vector2D {
public:
    float x;
    float y;

    Vector2D(float _x, float _y): x(_x), y(_y) {}
    Vector2D(const Point2D& point): x(point.x), y(point.y) {}
    Vector2D(const Point2D& p0, const Point2D& p1): x(p1.x - p0.x), y(p1.y - p0.y) {}
    Vector2D(const Point3D& point): x(point.x), y(point.y) {}
    Vector2D(float heading) {
        x = cos(heading);
        y = -sin(heading);
    }

    float sq_length() const {
        return sqr(x) + sqr(y);
    }

    float length() const {
        return sqrt(sq_length());
    }

    Vector2D rotated_ccw() const {
        return Vector2D(-y, x);
    }

    Vector2D rotated_cw() const {
        return Vector2D(y, -x);
    }

    Vector2D normalized() const {
        const float len = length();
        return Vector2D(x / len, y / len);
    }

    void operator*=(float scale) {
        x *= scale;
        y *= scale;
    }

    void operator+=(const Vector2D& other) {
        x += other.x;
        y += other.y;
    }

    Vector2D operator+(const Vector2D& other) const {
        return Vector2D(x + other.x, y + other.y);
    }
    Vector2D operator-(const Vector2D& other) const {
        return Vector2D(x - other.x, y - other.y);
    }
    Vector2D operator*(float scale) const {
        return Vector2D(x * scale, y * scale);
    }
};

inline Vector2D operator*(float scale, const Vector2D& vec) {
    return vec * scale;
}
inline Point2D operator+(const Point2D& point, const Vector2D& vec) {
    return Point2D(point.x + vec.x, point.y + vec.y);
}
inline Point2D operator-(const Point2D& point, const Vector2D& vec) {
    return Point2D(point.x - vec.x, point.y - vec.y);
}
inline float angle_mod(float a) {
    while(a > 2*PI) {
        a -= 2*PI;
    }
    while(a < 0) {
        a += 2*PI;
    }
    return a;
}
inline float heading(const Point2D& src, const Point2D& dest) {
    return angle_mod(atan2(src.y - dest.y, dest.x - src.x));
}
inline float angle_dist(float a0, float a1) {
    const float a = fabs(a0 - a1);
    return a > PI ? 2*PI - a : a;
}

#endif
