#include <iostream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <SDL/SDL.h>
#include "common.h"
#include "simulator.h"
#include "pathplan.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int DOT_RADIUS = 2;

Simulator sim;
PathPlanner planner(&sim);
SDL_Surface *screen;
std::vector<Point2D> user_dots;

inline void putPixel(int x, int y, int c) {
    if (x >= 0 && x < SCREEN_WIDTH && y >= 0 && y < SCREEN_HEIGHT) {
        ((unsigned int*)screen->pixels)[x + y * (screen->pitch/4)] = c;
    }
}

inline int screenX(float world_x) {
    return (int)((world_x * (float)(SCREEN_WIDTH)) / WORLD_WIDTH);
}

inline int screenY(float world_y) {
    return (int)((world_y * (float)(SCREEN_HEIGHT)) / WORLD_HEIGHT);
}

inline float worldX(int screen_x) {
    return ((float)(screen_x) * WORLD_WIDTH) / (float)(SCREEN_WIDTH);
}

inline float worldY(int screen_y) {
    return ((float)(screen_y) * WORLD_HEIGHT) / (float)(SCREEN_HEIGHT);
}

void swap(int& a, int& b) {
    int tmp = a;
    a = b;
    b = tmp;
}

void drawDot(int x, int y, int radius, int color) {
    for(int dx=-radius; dx<=radius; dx++) {
        for(int dy=-radius; dy<=radius; dy++) {
            if (dx*dx + dy*dy <= radius*radius) {
                putPixel(x+dx, y+dy, color);
            }
        }
    }
}

void drawLine(int x1, int y1, int x2, int y2, int color) {
    bool steep = abs(y2 - y1) > abs(x2 - x1);
    if (steep) {
        swap(x1, y1);
        swap(x2, y2);
    }
    if (x1 > x2) {
        swap(x1, x2);
        swap(y1, y2);
    }
    int deltax = x2 - x1;
    int deltay = abs(y2 - y1);
    int error = deltax / 2;
    int ystep = (y1 < y2 ? 1 : -1);
    
    int y = y1;
    for(int x=x1; x<=x2; x++) {
        if (steep) putPixel(y, x, color);
        else putPixel(x, y, color);
        
        error -= deltay;
        if (error < 0) {
            y += ystep;
            error += deltax;
        }
    }
}

void drawLine(float x1, float y1, float x2, float y2, int color) {
    drawLine(screenX(x1), screenY(y1), screenX(x2), screenY(y2), color);
}

void drawCar(const Car& car, int color = 0xFF0000) {
    Point2D car_corners[4];
    car.getCorners(car_corners);
    for(int i=0; i<4; i++) {
        int i2 = (i+1) % 4;
        drawLine(car_corners[i].x, car_corners[i].y, car_corners[i2].x, car_corners[i2].y, color);
    }
}

void refresh() {
    if (SDL_MUSTLOCK(screen)) {
        if (SDL_LockSurface(screen) < 0) {
            return;
        }
    }
    
    // Clear
    SDL_FillRect(screen, NULL, 0xEFEFEF);

    // Draw obstacle points
    for(size_t i=0; i<sim.obstacles_x.size(); i++) {
        drawDot(screenX(sim.obstacles_x[i]), screenY(sim.obstacles_y[i]), DOT_RADIUS, 0x888888);
    }
    for(size_t i=0; i<user_dots.size(); i++) {
        drawDot(screenX(user_dots[i].x), screenY(user_dots[i].y), DOT_RADIUS, 0x0000FF);
    }

    // Draw car
    drawCar(sim.car);

    if (planner.tree != NULL) {
        // Find best solution node
        std::vector<NodeInfo*> sol_nodes = planner.tree->neighbours_within_range(sim.target, 1.0);
        NodeInfo* best_sol = NULL;
        float best_error = FLT_MAX;
        for(size_t i=0; i<sol_nodes.size(); i++) {
            float heading_error = angle_dist(sol_nodes[i]->heading, sim.target.heading);
            if (heading_error < best_error) {
                best_error = heading_error;
                best_sol = sol_nodes[i];
            }
        }

        // Draw path to root
        NodeInfo* cur = best_sol;
        while (cur->parent != NULL) {
            drawLine(cur->parent->x, cur->parent->y, cur->x, cur->y, 0x000000);
            drawCar(Car(cur->x, cur->y, cur->heading));
            cur = cur->parent;
        }
        drawCar(Car(best_sol->x, best_sol->y, best_sol->heading), 0x00FF00);

        /*
        // Draw RRT tree
        std::vector<NodeInfo*> tree_nodes = planner.tree->all_nodes();
        for(int i=0; i<tree_nodes.size(); i++) {
            if (tree_nodes[i]->parent != NULL) {
                const NodeInfo cur = *(tree_nodes[i]);
                const NodeInfo parent = *(tree_nodes[i]->parent);
                //drawSpline(parent, cur, 0x000000);
                drawLine(parent.x, parent.y, cur.x, cur.y, 0x000000);
            }
        }
        */
    }
    
    if (SDL_MUSTLOCK(screen)) {
        SDL_UnlockSurface(screen);
    }
    SDL_UpdateRect(screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}

int main(int argc, char **argv) {
    srand(time(NULL));
    srand48(time(NULL));
    
    if (argc != 2) {
        std::cout << argv[0] << " " << "<map file>" << std::endl;
        return 0;
    }
    sim.loadMap(argv[1]);
    
    // SDL initialisation routines
    SDL_Init(SDL_INIT_VIDEO);
    atexit(SDL_Quit);
    screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_SWSURFACE);
    SDL_WM_SetCaption("Motion Planner", "Motion Planner");

    int mouse_x, mouse_y;
    while(1) {
        SDL_Event event;
        while(SDL_PollEvent(&event)) {
            switch(event.type) {
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym) {
                        case SDLK_r:
                            planner.plan();
                            break;
                        default:
                            break;
                    }
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    {
                        float dot_x = worldX(mouse_x);
                        float dot_y = worldY(mouse_y);
                        /*
                        user_dots.push_back(Point2D(dot_x, dot_y));
                        std::cout << dot_x << " " << dot_y << std::endl;
                        */
                        sim.obstacles_x.push_back(dot_x);
                        sim.obstacles_y.push_back(dot_y);
                    }
                    break;
                case SDL_QUIT:
                    return 0;
            }
        }

        SDL_GetMouseState(&mouse_x, &mouse_y);
        
        refresh();
        
        SDL_Delay(100);
    }

    return 0;
}
