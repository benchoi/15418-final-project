#ifndef _KDTREE_H_
#define _KDTREE_H_

#include <vector>
#include <queue>
#include "common.h"

#ifdef POINT_TYPE_2D
typedef Point2D Point;
#else
typedef NodeInfo Point;
#endif
const int DIM = 2;

class BestList;

class KDNode {
public:
    KDNode(const Point& _point, bool _point_valid = true, int dim = 0);
    ~KDNode();

    Point* get_point_ptr() {
        return &point;
    }
    Point* insert(const Point& new_point, bool new_point_valid = true);
    std::vector<Point*> all_nodes();
    std::vector<Point*> nearest_neighbours(const Point& target, size_t n);
    std::vector<Point*> neighbours_within_range(const Point& target, float range);

private:
    Point point;
    bool point_valid;
    KDNode* left_child;
    KDNode* right_child;
    int split_dim;

    void traverse(std::vector<Point*>& point_list);
    void nn_search(const Point& target, BestList& best_list);
    void range_search(const Point& target, float sq_range, std::vector<Point*>& point_list);
};

#endif
