CXX=g++ -m64 -march=native -msse
CXXFLAGS=-I../common -Iobjs/ -O3 -Wall -std=c++0x -fopenmp 
ISPC=ispc
ISPCFLAGS=-O2 --target=sse4-x2 --arch=x86-64

APP_NAME=motionplan
OBJDIR=objs

default: $(APP_NAME)

.PHONY: dirs clean

dirs:
		/bin/mkdir -p $(OBJDIR)/

clean:
		/bin/rm -rf $(OBJDIR) $(APP_NAME)

OBJS=$(OBJDIR)/main.o $(OBJDIR)/pathplan.o $(OBJDIR)/obsFree_ispc.o $(OBJDIR)/kdtree.o $(OBJDIR)/simulator.o 

$(APP_NAME): dirs $(OBJS)
		$(CXX) $(CXXFLAGS) -o $@ $(OBJS) -lSDL 

$(OBJDIR)/%_ispc.h $(OBJDIR)/%_ispc.o: %.ispc
		$(ISPC) $(ISPCFLAGS) $< -o $(OBJDIR)/$*_ispc.o -h $(OBJDIR)/$*_ispc.h

$(OBJDIR)/%.o: %.cpp
		$(CXX) $< $(CXXFLAGS) -c -lSDL -o $@

$(OBJDIR)/main.o: $(OBJDIR)/obsFree_ispc.h


