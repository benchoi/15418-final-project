//Thoughts: We can parallelize across vector lanes in a 2 different ways:
//1: Do 4 line tests for each obstacle point in parallel
//Benefit of 1: we know that we will always need to test at least 
//4 points. 
//For this method, we have to structure the car data 
//in a way such that the 4 x coordinates of the corners are together, 
//and the 4 y coordinates of the corners are together. 

/*
export uniform bool obsFree_ispc(uniform float cc_x[4], uniform float cc_y[4],
                        uniform float obs_x[], uniform float obs_y[], uniform int numobs) {
    uniform bool result = true;
    for(uniform int j = 0; j < numobs; j++){
        bool isoutside = false;
        float x1,x2;
        foreach(i = 0 ... 4){
                int i2 = (i+1) & 0x3; //(i+1) % 4
                float A = cc_y[i] - cc_y[i2];
                float B = cc_x[i2] - cc_x[i];
                float C = -B*cc_y[i] - A*cc_x[i];
                float D = A*obs_x[j] + B*obs_y[j] + C;
                if(D < 0) //No collision
                    isoutside = true;
        }
        result = result && any(isoutside);
    }
    return result;
}
*/

//2: Do multiple obstacle point in parallel
//Benefit of 2: is that we can utilize all 8 vector lanes, but if 
//there are very few close by obstacles, then underutilization might result
//We would need to preprocess points into array of x and y values.

export uniform bool obsFree_ispc(uniform float cc_x[4], uniform float cc_y[4], 
                                uniform float obs_x[], uniform float obs_y[], uniform int numobs) {
    for(uniform int j = 0; j < numobs; j += programCount) {
        int obs_idx = j + programIndex;
        bool outside = true;
        if (obs_idx < numobs) {
            outside = false;
            for(uniform int i = 0; i < 4; i++) {
                uniform int i2 = (i+1) & 0x3; //(i+1) % 4
                float A = cc_y[i] - cc_y[i2];
                float B = cc_x[i2] - cc_x[i];
                float C = -B*cc_y[i] - A*cc_x[i];
                float D = A*obs_x[obs_idx] + B*obs_y[obs_idx] + C;
                if (D < 0) {
                    outside = true;
                }
            }
        }
        if (!all(outside)) {
            return false;
        }
    }
    return true;
}
