#include <cmath>
#include "kdtree.h"

struct Candidate {
    Point* point;
    float dist;

    Candidate(Point* _point, float _dist): point(_point), dist(_dist) {}

    bool operator<(const Candidate& other) const {
        return dist < other.dist;
    }
};

class BestList: public std::priority_queue<Candidate> {
public:
    BestList(size_t _max_size): max_size(_max_size) {}
    
    void smart_push(const Candidate& elem) {
        if (this->size() < max_size) {
            this->push(elem);
        }
        else if (elem.dist < this->top().dist) {
            this->push(elem);
            this->pop();
        }
    }
    
    bool full() const {
        return this->size() == max_size;
    }
    
private:
    size_t max_size;
};

KDNode::KDNode(const Point& _point, bool _point_valid, int dim): 
point(_point), point_valid(_point_valid), left_child(NULL), right_child(NULL), split_dim(dim) {}

KDNode::~KDNode() {
    if (left_child != NULL) {
        delete left_child;
    }
    if (right_child != NULL) {
        delete right_child;
    }
}

inline Point* atomic_insert(KDNode*& ptr, const Point& new_point, bool new_point_valid, int next_dim) {
    if (ptr == NULL) {
        KDNode* new_node = new KDNode(new_point, new_point_valid, next_dim);
        if (!__sync_bool_compare_and_swap(&ptr, NULL, new_node)) {
            return ptr->insert(new_point, new_point_valid);
        }
        else {
            return ptr->get_point_ptr();
        }
    }
    else {
        return ptr->insert(new_point, new_point_valid);
    }
}

Point* KDNode::insert(const Point& new_point, bool new_point_valid) {
    const float* point_vals = &(point.x);
    const float* new_point_vals = &(new_point.x);
    const int next_dim = (split_dim + 1) % DIM;

    // Decide side on which the new point should go
    if (new_point_vals[split_dim] < point_vals[split_dim]) {
        return atomic_insert(left_child, new_point, new_point_valid, next_dim);
    }
    else {
        return atomic_insert(right_child, new_point, new_point_valid, next_dim);
    }
}

std::vector<Point*> KDNode::all_nodes() {
    std::vector<Point*> all_points;
    traverse(all_points);
    return all_points;
}

std::vector<Point*> KDNode::nearest_neighbours(const Point& target, size_t n) {
    BestList best_list(n);
    
    // Find n nearest neighbours
    nn_search(target, best_list);
    
    // Transfer results to vector
    std::vector<Point*> nearest;
    nearest.resize(best_list.size());
    while(best_list.size() > 0) {
        nearest[best_list.size()-1] = best_list.top().point;
        best_list.pop();
    }
    return nearest;
}

std::vector<Point*> KDNode::neighbours_within_range(const Point& target, float range) {
    std::vector<Point*> points;
    range_search(target, sqr(range), points);
    return points;
}

void KDNode::traverse(std::vector<Point*>& point_list) {
    point_list.push_back(&point);
    if (left_child != NULL) {
        left_child->traverse(point_list);
    }
    if (right_child != NULL) {
        right_child->traverse(point_list);
    }
}

void KDNode::nn_search(const Point& target, BestList& best_list) {
    if (left_child == NULL && right_child == NULL) {
        // Leaf
        if (point_valid) {
            best_list.smart_push(Candidate(&point, dist(point, target)));
        }
    }
    else {
        // Decide on main and alternate branches
        KDNode* main_branch = NULL;
        KDNode* alt_branch = NULL;
        const float* point_vals = &(point.x);
        const float* target_vals = &(target.x);
        if (target_vals[split_dim] < point_vals[split_dim]) {
            main_branch = left_child;
            alt_branch = right_child;
        }
        else {
            main_branch = right_child;
            alt_branch = left_child;
        }
        
        // Try to recur down main branch
        if (main_branch != NULL) {
            main_branch->nn_search(target, best_list);
        }
        
        // Add current node to best list if possible
        if (point_valid) {
            best_list.smart_push(Candidate(&point, dist(point, target)));
        }
        
        if (alt_branch != NULL) {
            // See if we must check alternate branch
            const float target_dist_to_plane = fabs(target_vals[split_dim] - point_vals[split_dim]);
            const float max_best_dist = (best_list.empty() ? FLT_MAX : best_list.top().dist);
            bool must_check_alt = (!best_list.full()) || (target_dist_to_plane < max_best_dist);
            if (must_check_alt) {
                alt_branch->nn_search(target, best_list);
            }
        }
    }
}

void KDNode::range_search(const Point& target, float sq_range, std::vector<Point*>& point_list) {
    if (left_child == NULL && right_child == NULL) {
        // Leaf
        if (point_valid && sq_dist(point, target) < sq_range) {
            point_list.push_back(&point);
        }
    }
    else {
        // Decide on main and alternate branches
        KDNode* main_branch = NULL;
        KDNode* alt_branch = NULL;
        const float* point_vals = &(point.x);
        const float* target_vals = &(target.x);
        if (target_vals[split_dim] < point_vals[split_dim]) {
            main_branch = left_child;
            alt_branch = right_child;
        }
        else {
            main_branch = right_child;
            alt_branch = left_child;
        }
        
        // Try to recur down main branch
        if (main_branch != NULL) {
            main_branch->range_search(target, sq_range, point_list);
        }
        
        // Add current node to point list if possible
        if (point_valid && sq_dist(point, target) < sq_range) {
            point_list.push_back(&point);
        }
        
        if (alt_branch != NULL) {
            // See if we must check alternate branch
            const float target_dist_to_plane = fabs(target_vals[split_dim] - point_vals[split_dim]);
            if (sqr(target_dist_to_plane) < sq_range) {
                alt_branch->range_search(target, sq_range, point_list);
            }
        }
    }
}
